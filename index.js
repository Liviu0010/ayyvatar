const {Worker} = require("worker_threads");
const Discord = require("discord.js");
const client = new Discord.Client();

const ownerID = "177445183695093760";
let trustedIDsEval = [ownerID];
let workers = [];

setInterval(workerTimeLimit, 1000);

client.on("ready", () => {
	console.log(`I AM ${client.user.tag}!`);
});

client.on("message", msg => {
	if(msg.content[0] == "$") {
		let originalCommand = msg.content.slice(1, msg.content.length);
		let command = originalCommand.toLowerCase();
		
		if(command.slice(0, "avatar".length) == "avatar") {
			if(msg.mentions.users.first()) {
				msg.channel.send(`Avatar for ${msg.mentions.users.first()} \n`
								+`${msg.mentions.users.first().avatarURL}`);
			}
			else {
				msg.channel.send(msg.author.avatarURL);
			}
		}
		
		if(command.slice(0, "eval".length) == "eval") {
			if(trustedIDsEval.includes(msg.author.id)) {
				try {
					//get rid of the code tags if there are any
					let expression = originalCommand.slice("eval".length, originalCommand.length)
					.split("```js").join("").split("```").join("");
					
					evalWorker(expression, msg.channel, msg.author)
					.then((result) => msg.channel.send(result))
					.catch((e) =>{console.log(e); msg.reply(e.message)});
				}
				catch(e) {
					msg.reply(e.message);
				}
			}
			else {
				msg.reply("UNAUTHORIZED!!");
			}
		}
		
		if(command.slice(0, "addtrustedeval".length) == "addtrustedeval") {
			if(msg.author.id == ownerID) {
				//example: $addTrustedEval 12341241141
				let toAdd = command.split(" ")[1];
				trustedIDsEval.push(toAdd);
				msg.reply(`${toAdd} added. Trusted IDs for eval so far: ${trustedIDsEval}`);
			}
			else {
				msg.reply("UNAUTHORIZED!!!");
			}
		}
		
		if(command.slice(0, "removetrustedeval".length) == "removetrustedeval") {
			if(msg.author.id == ownerID) {
				//example: $removeTrustedEval 12341241141
				let toRemove = command.split(" ")[1];
				trustedIDsEval = trustedIDsEval.filter((id) => id != toRemove);
				msg.reply(`${toRemove} removed. Trusted IDs for eval so far: " + ${trustedIDsEval}`);
			}
			else {
				msg.reply("UNAUTHORIZED!!!");
			}
		}
		
		if(command.slice(0, "showtrustedeval".length) == "showtrustedeval") {
			if(msg.author.id == ownerID) {
				//example: $showtrustedeval
				msg.reply("Trusted IDs for eval so far: " + trustedIDsEval);
			}
			else {
				msg.reply("UNAUTHORIZED!!!");
			}
		}
	}
});

client.login(process.env.API_KEY);

function evalWorker(expression, chnel, athr) {
	return new Promise( (resolve, reject) => {
		let worker = new Worker("./evalWorker.js", {workerData: expression});
		workers.push({thread: worker, startTime: (new Date()).getTime(), 
			channel: chnel, author: athr});
		worker.on("message", (msg)=> {
			resolve(msg);
			workers = workers.filter( (obj) => obj.thread != worker);
		});
		worker.on("error", (msg) => {
			reject(msg);
			workers = workers.filter( (obj) => obj.thread != worker);
		});
	});
}

//killing workers that would take too long to finish executing
function workerTimeLimit() {
	let timeLimit = 60000;
	let currentTime = new Date().getTime();
	
	workers = workers.filter( (obj) => {
				if(currentTime - obj.startTime > timeLimit) {
					obj.thread.terminate();
					obj.channel.send(`${obj.author.toString()}, your worker got terminated for exceeding the time limit! (${timeLimit/1000}s)`);
					return false;
				}
				else {
					return true;
				}
			});
}