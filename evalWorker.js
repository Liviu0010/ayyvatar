//worker for eval
//since worker threads don't directly share memory, there's no need to worry about
//keeping unneeded stuff in memory from some random eval or people accessing stuff they should not
const {workerData, parentPort} = require("worker_threads");

parentPort.postMessage(eval(workerData));